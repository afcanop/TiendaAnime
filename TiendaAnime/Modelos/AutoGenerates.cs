﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web;

namespace TiendaAnime.Modelos
{
    public partial class Producto
    {
        public Producto(bool autogenerar) {
            this._Categoria = default(EntityRef<Categoria>);
            OnCreated();
            this.Creado = DateTime.Now;
            this.Estado = true;
            this.UrlImagen = "Imagenes/Imagen-no-disponible.png";
        }
    }

    public partial class Categoria
    {
        public Categoria(bool autogenerar)
        {
            this._Producto = new EntitySet<Producto>(new Action<Producto>(this.attach_Producto), new Action<Producto>(this.detach_Producto));
            OnCreated();

            this.Estado = true;
        }
    }

    public partial class Cliente
    {
        public Cliente(bool autogenerar)
        {
            OnCreated();

            this.Estado = true;
        }
    }

}