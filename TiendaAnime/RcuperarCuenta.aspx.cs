﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TiendaAnime.Modelos;
using SimpleCrypto;

namespace TiendaAnime
{
    public partial class RcuperarCuenta : System.Web.UI.Page
    {
        private TiendaAnimeDataContext db = new TiendaAnimeDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnIngreso_Click(object sender, EventArgs e)
        {
            string correo = txtEmil.Text.Trim();

            if (correo != null)
            {
                var Consulta = db.Cliente.Where(X => X.CorreoElectronico == correo).FirstOrDefault();

                if (Consulta != null)
                {
                    ICryptoService cryptoService = new PBKDF2();
                    string ContrasenaNueva = RandomPassword.Generate(10,PasswordGroup.Lowercase,PasswordGroup.Uppercase);

                    string NuevoSalt = cryptoService.GenerateSalt();
                    string ContrasenaEncriptada = cryptoService.Compute(ContrasenaNueva);

                    try
                    {
                        Consulta.Salt = NuevoSalt;
                        Consulta.Contrasena = ContrasenaEncriptada;
                        db.SubmitChanges();
                    }
                    catch (Exception)
                    {

                        throw;
                    }                     
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(
                      this.Page, this.GetType(), "AlertaFallidaLogin",
                      "window.onload=function(){  alertify.error('Usuario no existe');};", true);
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(
                      this.Page, this.GetType(), "AlertaFallidaLogin",
                      "window.onload=function(){  alertify.error('Los campos no puen ser vacios');};", true);
            }

        }
    }
}