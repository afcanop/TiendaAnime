﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TiendaAnime.Modelos;

namespace TiendaAnime
{
    public partial class SeriesM : System.Web.UI.Page
    {
        private TiendaAnimeDataContext db = new TiendaAnimeDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void GuardarBtn_Click(object sender, EventArgs e)
        {
            Modelos.Producto objserie = new Modelos.Producto(true);
            objserie.Nombre = txtNombreSerie.Text;
            objserie.Descripcion = txtDescripcion.Text;
            objserie.Precio = Decimal.Parse(txtPrecio.Text);
            objserie.UrlTrailer = txtTrailer.Text;
            objserie.NumeroCapitulos = int.Parse(txtNumeroCapitulos.Text);
            objserie.NumeroTemporadas = int.Parse(txtNumeroTemporada.Text);
            objserie.NumeroTemporadas = int.Parse(txtTotalTemporadas.Text);
            objserie.IdCategoria = int.Parse(txtCategorias.SelectedItem.Value);

            try
            {
                db.Producto.InsertOnSubmit(objserie);
                db.SubmitChanges();

            }
            catch (Exception)
            {

                throw;
            }
         

        }

        protected void txtNumeroCapitulos_TextChanged(object sender, EventArgs e)
        {

        }
    }
}