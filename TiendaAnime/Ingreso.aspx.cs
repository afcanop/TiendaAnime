﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using TiendaAnime.Modelos;
using SimpleCrypto;

namespace TiendaAnime

{
    public partial class Ingreso : System.Web.UI.Page
    {
        private TiendaAnimeDataContext db = new TiendaAnimeDataContext();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Context.User.Identity.IsAuthenticated)
                {
                    Response.Redirect("Index.aspx");
                }
            }
        }

        protected void btnIngreso_Click(object sender, EventArgs e)
        {
            string Email = txtNombre.Text.Trim();
            string Contrasena = txtContrasena.Text.Trim();

           
           

            if (Email != "" && Contrasena != "")
            {
                var consulta = db.Cliente.Where(x=> x.CorreoElectronico == Email).FirstOrDefault();
                ICryptoService encritar = new PBKDF2();
                if (consulta != null)
                {
                    string passEncritada = encritar.Compute(Contrasena,consulta.Salt);
                    if (encritar.Compare(passEncritada, consulta.Contrasena))
                    {
                        string NombreCompleto = consulta.PriemeNombre + " " + consulta.SegundoNombre;
                        FormsAuthentication.SetAuthCookie(NombreCompleto,true);
                        Response.Redirect("Index.aspx");
                    }
                    else
                    { ScriptManager.RegisterClientScriptBlock(
                    this.Page,this.GetType(),"AlertaFallidaLogin",
                    "window.onload=function(){  alertify.error('contraseña invalida');};", true);
                            
                    }

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(
                    this.Page,this.GetType(),"AlertaFallidaLogin",
                    "window.onload=function(){  alertify.error('Email es invalido');};", true);     
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(
                    this.Page,this.GetType(),"AlertaFallidaLogin",
                    "window.onload=function(){  alertify.error('Los campos de usuarios y contraseña no puen ser vacios');};", true);
            }


        }
    }
}