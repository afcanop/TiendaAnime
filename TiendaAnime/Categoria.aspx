﻿<%@ Page Title="Categoria" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Categoria.aspx.cs" Inherits="TiendaAnime.Formulario_web1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Css" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Contenido" runat="server">
    <div class="container">
        <h3 class="text-center">Registro nueva categoria</h3>
        <div class="form-group text-center">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <label class="control-label" for="txtCategoria">Nombre Categoria</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <samp class="glyphicon glyphicon-pencil"></samp>
                        </span>
                        <asp:TextBox ID="txtCategoria" runat="server" CssClass="form-control input-md"></asp:TextBox>
                    </div>
                </div>
            </div>
                   <br />
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <asp:LinkButton CssClass="btn btn-success glyphicon glyphicon-floppy-disk" ID="GuardarBtn" runat="server" OnClick="GuardarBtn_Click"> Guardar</asp:LinkButton>
                </div>
            </div>
            <br />
            <h3 class="text-center">Tabla categoria</h3>
            <div class="row">
                <div class="col-md-12 ">
                    <asp:GridView ID="TablaCategorias" runat="server" CssClass="table table-hover table-bordered table-responsive text-center" 
                        AutoGenerateColumns="False" DataKeyNames="IdCategoria" DataSourceID="TablaCategoria" 
                        AllowSorting="true" AutoGenerateEditButton="true"  AllowPaging="true" PageSize="20"> 
                        <Columns>
                            <asp:BoundField DataField="IdCategoria" HeaderText="Codigo Categoria" InsertVisible="False" ReadOnly="True" SortExpression="IdCategoria" />
                            <asp:BoundField DataField="Nombre" HeaderText="Nombre Categoria" SortExpression="Nombre" />
                            <asp:CheckBoxField DataField="Estado" HeaderText="Estado de la categoria" SortExpression="Estado" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID ="btnEliminar" CssClass="btn btn-danger " runat="server" 
                                        CommandName="Delete" OnClientClick="return ConfirmarDelete();">
                                        <i class="glyphicon glyphicon-remove"></i>   
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>                        
                        </Columns>
                        <EmptyDataTemplate>
                            <div class="jumbotron">
                                <h1>No hay Categorias Registradas, por el momento</h1>
                            </div>
                        </EmptyDataTemplate>
                    </asp:GridView>
                    <asp:SqlDataSource ID="TablaCategoria" runat="server" ConnectionString="<%$ ConnectionStrings:TiendaAnimeConnectionString2 %>" 
                        SelectCommand="SELECT [IdCategoria], [Nombre], [Estado] FROM [Categoria] ORDER BY [IdCategoria] DESC"
                        UpdateCommand="update Categoria set Nombre = @Nombre ,Estado= @Estado where  IdCategoria = @IdCategoria"
                        DeleteCommand="delete categoria where IdCategoria = @IdCategoria"
                        >
                        <UpdateParameters>
                            <asp:Parameter  Name="Nombre" Type="String" Size="100"/>
                            <asp:Parameter  Name="Estado" Type="Boolean" />
                        </UpdateParameters>
                        <DeleteParameters>
                            <asp:Parameter  Name="IdCategoria" Type="Int32"/>
                        </DeleteParameters>
                    </asp:SqlDataSource>
                </div>
            </div>

        </div>

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script>
        
        function ConfirmarDelete()
        {


            respuesta = confirm('Confirm Title', 'Confirm Message')


            return respuesta;
        }
    </script>

</asp:Content>
