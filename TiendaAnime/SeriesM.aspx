﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="SeriesM.aspx.cs" Inherits="TiendaAnime.SeriesM" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenido" runat="server">
    <div class="container">
        <h3 class="text-center">Registro nuevo Serie</h3>
        <div class="form-group text-center">
            <div class="row">
                <div class="col-md-6">
                    <label class="control-label" for="txtNombreSerie">Nombre de la serie</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <samp class="glyphicon glyphicon-pencil"></samp>
                        </span>
                        <asp:TextBox ID="txtNombreSerie" runat="server" CssClass="form-control input-md"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-6">
                    <label class=" control-label" for="txtNumeroTemporada">Numero de temporada</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <samp class="glyphicon glyphicon-pencil"></samp>
                        </span>
                        <asp:TextBox ID="txtNumeroTemporada" runat="server" CssClass="form-control input-md" TextMode="Number"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label class="control-label" for="txtNumeroCapitulos">Numero de capitulos</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <samp class="glyphicon glyphicon-pencil"></samp>
                        </span>
                        <asp:TextBox ID="txtNumeroCapitulos" runat="server" CssClass="form-control input-md" TextMode="Number" OnTextChanged="txtNumeroCapitulos_TextChanged"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-6">
                    <label class=" control-label" for="ImgSerie">
                        Imagen de la serie
                    </label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <samp class="glyphicon glyphicon-picture"></samp>
                        </span>
                        <asp:FileUpload ID="ImgSerie" runat="server" CssClass="btn  btn-default" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label class="control-label" for="txtPrecio">Precio</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <samp class="glyphicon glyphicon-pencil"></samp>
                        </span>
                        <asp:TextBox ID="txtPrecio" runat="server" CssClass="form-control input-md" TextMode="Number"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="control-label" for="txtTrailer">Trailer</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <samp class="glyphicon glyphicon-pencil"></samp>
                        </span>
                        <asp:TextBox ID="txtTrailer" runat="server" CssClass="form-control input-md" TextMode="Number"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-md-6">
                <label class="control-label" for="txtCategorias">Categorias</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <samp class="glyphicon glyphicon-list-alt"></samp>
                    </span>
                    <asp:DropDownList ID="txtCategorias" runat="server" CssClass="form-control" DataSourceID="SqlDataSource1" DataTextField="Nombre" DataValueField="IdCategoria">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TiendaAnimeConnectionString2 %>" SelectCommand="SELECT [Nombre], [IdCategoria] FROM [Categoria]"></asp:SqlDataSource>
                </div>
            </div>
            <div class="col-md-6">
                <label class="control-label" for="txtNomClinte">Total de temporadas</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <samp class="glyphicon glyphicon-pencil"></samp>
                    </span>
                    <asp:TextBox ID="txtTotalTemporadas" runat="server" CssClass="form-control input-md" TextMode="Number"></asp:TextBox>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <label class="control-label" for="txtNomClinte">Descripción Serie</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <samp class="glyphicon glyphicon-pencil"></samp>
                    </span>
                    <asp:TextBox ID="txtDescripcion" runat="server" CssClass="form-control input-md" TextMode="MultiLine"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <asp:LinkButton CssClass="btn btn-success glyphicon glyphicon-floppy-disk" ID="GuardarBtn" runat="server" OnClick="GuardarBtn_Click"> Guardar</asp:LinkButton>
            </div>
        </div>
        <h3>Tabla de series </h3>
        <div class="row">
            <div class="col-md-12">
                <asp:GridView ID="tablaSerie" runat="server" CssClass="table table-hover table-bordered table-responsive text-center" AllowSorting="True" AutoGenerateEditButton="True" AllowPaging="True" PageSize="20" AutoGenerateColumns="False" DataKeyNames="IdProducto" DataSourceID="TablaSeriem">
                    <Columns>
                        <asp:BoundField DataField="IdProducto" HeaderText="IdProducto" InsertVisible="False" ReadOnly="True" SortExpression="IdProducto" />
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre" SortExpression="Nombre" />
                        <asp:BoundField DataField="Precio" HeaderText="Precio" SortExpression="Precio" />
                        <asp:BoundField DataField="Cantidad" HeaderText="Cantidad" SortExpression="Cantidad" />
                        <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" SortExpression="Descripcion" />
                        <asp:BoundField DataField="UrlTrailer" HeaderText="UrlTrailer" SortExpression="UrlTrailer" />
                        <asp:BoundField DataField="UrlImagen" HeaderText="UrlImagen" SortExpression="UrlImagen" />
                        <asp:BoundField DataField="NumeroTemporada" HeaderText="NumeroTemporada" SortExpression="NumeroTemporada" />
                        <asp:BoundField DataField="NumeroCapitulos" HeaderText="NumeroCapitulos" SortExpression="NumeroCapitulos" />
                        <asp:BoundField DataField="Creado" HeaderText="Creado" SortExpression="Creado" />
                        <asp:BoundField DataField="Modificado" HeaderText="Modificado" SortExpression="Modificado" />
                        <asp:BoundField DataField="Eliminado" HeaderText="Eliminado" SortExpression="Eliminado" />
                        <asp:CheckBoxField DataField="Estado" HeaderText="Estado" SortExpression="Estado" />
                        <asp:BoundField DataField="IdCategoria" HeaderText="IdCategoria" SortExpression="IdCategoria" />
                    </Columns>


                </asp:GridView>
                <asp:SqlDataSource ID="TablaSeriem" runat="server" ConnectionString="<%$ ConnectionStrings:TiendaAnimeConnectionString2 %>" SelectCommand="SELECT [IdProducto], [Nombre], [Precio], [Cantidad], [Descripcion], [UrlTrailer], [UrlImagen], [NumeroTemporada], [NumeroCapitulos], [Creado], [Modificado], [Eliminado], [Estado], [IdCategoria] FROM [Producto] ORDER BY [IdProducto] DESC, [IdCategoria] DESC"></asp:SqlDataSource>
            </div>
        </div>
    </div>

</asp:Content>

