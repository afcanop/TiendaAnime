﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Clientes.aspx.cs"
    Inherits="TiendaAnime.Clientes" MasterPageFile="~/MasterPage.Master" %>


<asp:Content ID="Content2" ContentPlaceHolderID="Contenido" runat="server">
    <!--formulario -->
    <div class="container">
        <h3 class="text-center">Registro nuevo cliente</h3>
        <div class="form-group text-center">
            <div class="row">
                <div class="col-md-6">
                    <label class="control-label" for="txtPrimerNombre">Primer Nombre</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <samp class="glyphicon glyphicon-pencil"></samp>
                        </span>
                        <asp:TextBox ID="txtPrimerNombre" runat="server" CssClass="form-control input-md"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="control-label" for="txtSegundoNombre">Segundo Nombre</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <samp class="glyphicon glyphicon-pencil"></samp>
                        </span>
                        <asp:TextBox ID="txtSegundoNombre" runat="server" CssClass="form-control input-md"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label class="control-label" for="txtPrimerApellido">Primer Apellido</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <samp class="glyphicon glyphicon-pencil"></samp>
                        </span>
                        <asp:TextBox ID="txtPrimerApellido" runat="server" CssClass="form-control input-md"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="control-label" for="txtSegundoApellido">Segundo Apellido</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <samp class="glyphicon glyphicon-pencil"></samp>
                        </span>
                        <asp:TextBox ID="txtSegundoApellido" runat="server" CssClass="form-control input-md"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label class="control-label" for="txtEmail">Email</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <samp class="glyphicon glyphicon-pencil"></samp>
                        </span>
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control input-md" TextMode="Email"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="control-label" for="txtNumeroContacto">Número contacto</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <samp class="glyphicon glyphicon-pencil"></samp>
                        </span>
                        <asp:TextBox ID="txtNumeroContacto" runat="server" CssClass="form-control input-md" TextMode="Number" min="1"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label class="control-label" for="txtNumeroIdentificacion">Número de Identifiación</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <samp class="glyphicon glyphicon-pencil"></samp>
                        </span>
                        <asp:TextBox ID="txtNumeroIdentificacion" runat="server" CssClass="form-control input-md" TextMode="Number" min="1"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="control-label" for="txtContrasena">Contraseña</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <samp class="glyphicon glyphicon-pencil"></samp>
                        </span>
                        <asp:TextBox ID="txtContrasena" runat="server" CssClass="form-control input-md"></asp:TextBox>
                    </div>
                </div>
            </div>

            <br />
            <asp:LinkButton ID="btnRegistrar" runat="server" Text="Registar" CssClass="btn btn-success text-center" OnClick="btnRegistrar_Click">
                    <i class="glyphicon glyphicon-floppy-disk"> </i>    Registar
                    
            </asp:LinkButton>
            <br />

            <h3>Lista de clientes
            </h3>
            <div class="row">
                <div class="col-md-12">
                    <asp:GridView CssClass="table table-hover table-bordered table-responsive text-center"
                        ID="TablaCLientes" runat="server" AutoGenerateColumns="False" DataKeyNames="IdCliente" DataSourceID="TablasClientes"
                        AllowSorting="true" AutoGenerateEditButton="true" AllowPaging="true" PageSize="20">
                        <Columns>
                            <asp:BoundField DataField="IdCliente" HeaderText="IdCliente" InsertVisible="False" ReadOnly="True" SortExpression="IdCliente" />
                            <asp:BoundField DataField="PriemeNombre" HeaderText="PriemeNombre" SortExpression="PriemeNombre" />
                            <asp:BoundField DataField="SegundoNombre" HeaderText="SegundoNombre" SortExpression="SegundoNombre" />
                            <asp:BoundField DataField="PrimerApellido" HeaderText="PrimerApellido" SortExpression="PrimerApellido" />
                            <asp:BoundField DataField="SegundoApellido" HeaderText="SegundoApellido" SortExpression="SegundoApellido" />
                            <asp:BoundField DataField="CorreoElectronico" HeaderText="CorreoElectronico" SortExpression="CorreoElectronico" />
                            <asp:BoundField DataField="NumeroContacto" HeaderText="NumeroContacto" SortExpression="NumeroContacto" />
                            <asp:BoundField DataField="NumeroIdentificacion" HeaderText="NumeroIdentificacion" SortExpression="NumeroIdentificacion" />
                            <asp:CheckBoxField DataField="Estado" HeaderText="Estado" SortExpression="Estado" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnEliminar" CssClass="btn btn-danger " runat="server"
                                        CommandName="Delete" OnClientClick="return ConfirmarDelete();">
                                        <i class="glyphicon glyphicon-remove"></i>   
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            <div class="jumbotron">
                                <h1>No hay Categorias Registradas, por el momento</h1>
                            </div>
                        </EmptyDataTemplate>
                    </asp:GridView>
                    <asp:SqlDataSource ID="TablasClientes" runat="server" ConnectionString="<%$ ConnectionStrings:TiendaAnimeConnectionString2 %>" 
                        SelectCommand="SELECT [IdCliente], [PriemeNombre], [SegundoNombre], [PrimerApellido], [SegundoApellido], [CorreoElectronico], [NumeroContacto], [NumeroIdentificacion], [Estado] FROM [Cliente] ORDER BY [IdCliente] DESC"
                        UpdateCommand="update Cliente set PriemeNombre=@PriemeNombre, SegundoNombre=@SegundoNombre, PrimerApellido=@PrimerApellido, SegundoApellido=@SegundoApellido, CorreoElectronico=@CorreoElectronico,NumeroContacto=@NumeroContacto,Estado=@Estado where IdCliente=@IdCliente "
                        DeleteCommand="delete Cliente where IdCliente = @IdCliente"
                        >
                           <UpdateParameters>
                             <asp:Parameter  Name="IdCliente" Type="Int32"/>
                            <asp:Parameter  Name="PriemeNombre" Type="String" Size="100"/>
                            <asp:Parameter  Name="SegundoNombre" Type="String" Size="100"/>
                            <asp:Parameter  Name="PrimerApellido" Type="String" Size="100"/>
                            <asp:Parameter  Name="SegundoApellido" Type="String" Size="100"/>
                            <asp:Parameter  Name="CorreoElectronico" Type="String" Size="100"/>
                            <asp:Parameter  Name="NumeroContacto" Type="String" Size="100"/>
                            <asp:Parameter  Name="Estado" Type="Boolean" />
                        </UpdateParameters>
                         <DeleteParameters>
                            <asp:Parameter  Name="IdCliente" Type="Int32"/>
                        </DeleteParameters>
                    </asp:SqlDataSource>

                
                </div>
            </div>

        </div>
    </div>




    <!--end formulario -->

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script>
        
        function ConfirmarDelete()
        {


            respuesta = confirm('Confirm Title', 'Confirm Message')


            return respuesta;
        }
    </script>

</asp:Content>

