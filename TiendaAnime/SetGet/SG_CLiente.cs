﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TiendaAnime.SetGet
{
    public class SG_CLiente
    {
        public int IdCLiente { get; set; }
        public String PrimerNombre { get; set; }
        public String SegundoNombre { get; set; }
        public String PrimerApellido { get; set; }
        public String SegundoApellido { get; set; }
        public String Email { get; set; }
        public int NumeroConTacto { get; set; }
        public int NumeroIdentificacion { get; set; }

    }
}