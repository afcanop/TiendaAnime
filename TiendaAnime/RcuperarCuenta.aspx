﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Login.Master" AutoEventWireup="true" CodeBehind="RcuperarCuenta.aspx.cs" Inherits="TiendaAnime.RcuperarCuenta" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Css" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Contenido" runat="server">
    
        <div class="col-sm-6 col-md-4 col-md-offset-4">

            <div class="account-wall">
                <h1 class="text-center login-title">Ingreso</h1>
                <img class="profile-img img-responsive img-circle" src="Imagenes/user.png" alt="" />
                <div class="form-signin">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <samp class="glyphicon glyphicon-user"></samp>
                        </span>
                        <asp:TextBox ID="txtEmil" runat="server" CssClass="form-control input-md" placeholder="Ejm: juan@gmail.com" TextMode="Email"></asp:TextBox>
                    </div>
                    <br />
                    <asp:Button ID="btnIngreso" CssClass="btn btn-lg btn-primary btn-block" runat="server" Text="Recuperar" OnClick="btnIngreso_Click" />
                    <a href="Ingreso.aspx" class="pull-right need-help">Cancelar </a><span class="clearfix"></span>
                </div>
            </div>
        </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
