﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Productos.aspx.cs" Inherits="TiendaAnime.Productos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Css" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Contenido" runat="server">
    
      <div class="container">
            <h3 class="text-center">Registro nuevo Producto</h3>
            <div class="form-group text-center">
                <div class="row">
                    <div class="col-md-6">
                        <label class="control-label" for="txtNomClinte">Nombre del Producto</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <samp class="glyphicon glyphicon-pencil"></samp>
                            </span>
                            <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control input-md"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class=" control-label" for="txtApellido">Cantidad</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <samp class="glyphicon glyphicon-pencil"></samp>
                            </span>
                            <asp:TextBox ID="txtCantidad" runat="server" CssClass="form-control input-md" TextMode="Number"></asp:TextBox>
                        </div>
                    </div>
                </div>
                  <div class="row">
                    <div class="col-md-6">
                        <label class="control-label" for="txtNomClinte">Precio</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <samp class="glyphicon glyphicon-pencil"></samp>
                            </span>
                            <asp:TextBox ID="TextPrecio" runat="server" CssClass="form-control input-md" TextMode="Number"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class=" control-label" for="txtApellido">
                            Imagen del producto
                        </label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <samp class="glyphicon glyphicon-picture"></samp>
                            </span>
                            <asp:FileUpload ID="FileUpload1" runat="server" CssClass="btn  btn-default" />
                        </div>
                    </div>
                  
                </div>
            </div>
      </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
