﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TiendaAnime.Modelos;
using SimpleCrypto;

namespace TiendaAnime
{
    public partial class Clientes : System.Web.UI.Page
    {


        private TiendaAnimeDataContext db = new TiendaAnimeDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            Modelos.Cliente objCliente = new Modelos.Cliente(true);

            ICryptoService encritar = new PBKDF2();

            //algoritmo de encritar
            string salt = encritar.GenerateSalt();

            string passEncritada = encritar.Compute(txtContrasena.Text);


            objCliente.PriemeNombre = txtPrimerNombre.Text;
            objCliente.SegundoNombre = txtSegundoNombre.Text;
            objCliente.PrimerApellido = txtPrimerApellido.Text;
            objCliente.SegundoApellido = txtSegundoApellido.Text;
            objCliente.CorreoElectronico = txtEmail.Text;
            objCliente.NumeroContacto = txtNumeroContacto.Text;
            objCliente.NumeroIdentificacion = txtNumeroIdentificacion.Text;
            objCliente.Contrasena = passEncritada;
            objCliente.Salt = salt;

            txtPrimerNombre.Text="";
            txtSegundoNombre.Text="";
            txtPrimerApellido.Text = "";
            txtSegundoApellido.Text = "";
            txtEmail.Text = "";
            txtEmail.Text = "";
            txtNumeroIdentificacion.Text = "";
            txtContrasena.Text = "";

            try
            {

                db.Cliente.InsertOnSubmit(objCliente);
                db.SubmitChanges();

                    ScriptManager.RegisterClientScriptBlock(
                    this.Page,this.GetType(),"AlertaFallidaLogin",
                    "window.onload=function(){  alertify.success('REGISTRO COMPLETO');};", true);

            }
            catch (Exception)
            {

                throw;
            }

        }
    }
}