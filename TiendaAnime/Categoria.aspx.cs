﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TiendaAnime.Modelos;

namespace TiendaAnime
{
    public partial class Formulario_web1 : System.Web.UI.Page
    {
        private TiendaAnimeDataContext db = new TiendaAnimeDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void GuardarBtn_Click(object sender, EventArgs e)
        {
            Categoria objCategoria = new Categoria(true);

            objCategoria.Nombre = txtCategoria.Text;

            try
            {
                db.Categoria.InsertOnSubmit(objCategoria);
                db.SubmitChanges();

                txtCategoria.Text = "";
                ScriptManager.RegisterClientScriptBlock(this.Page,this.Page.GetType(),"alerta excitosa", "Window.onload = function(){  alertify.success(\'Registro corecta mente\');} ", true);
        

            }
            catch (Exception)
            {

                throw;
            }

           

        }
    }
}